#!/bin/bash
echo "Date;Description;Debit;Credit;Balance" > ledger.csv
tail -n +2 ledger-unsorted.csv | sort -t "-" -k1n -k2n -k3n >> ledger.csv