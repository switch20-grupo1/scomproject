function getFamilyMembers() {
  const URL_MEMBER_BASENAME = "sws.family.demo";
  var request = new XMLHttpRequest();
  request.onload = function getMembers() {
    const users = JSON.parse(this.responseText);
    const members = document.getElementById("member");
    members.remove(0);
    users.forEach(({ vat, member }) => {
      let option = document.createElement("option");
      option.value = vat;
      option.text = member + " (" + vat + ")";
      members.add(option);
    });
    members.disabled = false;
    // setTimeout(getFamilyMembers, 1500);
  };
  request.ontimeout = function timeoutCase() {
    document.getElementById("members").innerHTML = "Still trying ...";
    setTimeout(getFamilyMembers, 1000);
  };
  request.onerror = function errorCase() {
    document.getElementById("members").innerHTML = "Still trying ...";
    setTimeout(getFamilyMembers, 1000);
  };
  request.open("GET", "/cgi-bin/familyMembers", true);
  request.timeout = 5000;
  request.send();
}

function connectFamilyMember() {
  if (checkInputs()) {
    const memberVAT = document.getElementById("member").value; // Changed field type
    const service = document.getElementById("service").value; // Changed field type
    const uri = document.getElementById("uri").value; // Changed field type
    let member = JSON.stringify({
      member: Number(memberVAT),
      service: service,
      uri: uri,
    });
    console.log("Connecting member " + member);
    const request = new XMLHttpRequest();
    request.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        window.location.href = "/sws-family.html";
      }
    });
    request.open("POST", "/cgi-bin/connectServices");
    request.setRequestHeader("Content-Type", "application/json");
    request.send(member);
  }
}

function checkURLFormat(string) {
  var pattern = new RegExp(
    "^(https?:\\/\\/)?" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
    "i"
  ); // fragment locator
  return !!pattern.test(string);
}
function checkSelectorValue(selector) {
  return selector.value != "";
}
function checkInputs() {
  const uri = document.getElementById("uri");
  const member = document.getElementById("member");
  const service = document.getElementById("service");
  let status = true;
  if (!checkURLFormat(uri.value)) {
    setErrorFor(uri, "URI is invalid");
    status = false;
  } else {
    setSuccessFor(uri);
  }
  if (!checkSelectorValue(member)) {
    setErrorFor(member, "Member wasn't selected");
    status = false;
  } else {
    setSuccessFor(member);
  }
  if (!checkSelectorValue(service)) {
    setErrorFor(service, "No Service was selected");
    status = false;
  } else {
    setSuccessFor(service);
  }
  return status;
}
function setErrorFor(input, message) {
  const formControl = input.parentElement;
  const errorMessage = input.parentElement.getElementsByClassName(
    "error-message"
  )[0]; //get first and only object

  errorMessage.innerText = message;
  formControl.classList.add("error");
  errorMessage.classList.add("error");
}
function setSuccessFor(input) {
  const formControl = input.parentElement;
  const errorMessage = input.parentElement.getElementsByClassName(
    "error-message"
  )[0]; //get first and only object
  formControl.classList.remove("error");
  formControl.classList.add("success");
  errorMessage.classList.remove("error");
}
