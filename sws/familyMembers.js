function getFamilyMembers() {
  const URL_MEMBER_BASENAME = "sws.family.demo";
  var request = new XMLHttpRequest();
  request.onload = function getMembers() {
    const users = JSON.parse(this.responseText);
    const members = document.getElementById("members");
    members.innerHTML = "";
    users.forEach(({ vat, member }) => {
      members.insertAdjacentHTML(
        "beforeend",
        `<h3><a class="member text-primary" href="${URL_MEMBER_BASENAME}/${vat}.html">${member}</a></h3>`
      );
    });
    // setTimeout(getFamilyMembers, 1500);
  };
  request.ontimeout = function timeoutCase() {
    document.getElementById("members").innerHTML = "Still trying ...";
    setTimeout(getFamilyMembers, 1000);
  };
  request.onerror = function errorCase() {
    document.getElementById("members").innerHTML = "Still trying ...";
    setTimeout(getFamilyMembers, 1000);
  };
  request.open("GET", "/cgi-bin/familyMembers", true);
  request.timeout = 5000;
  request.send();
}
function createFamilyMember() {
  if (checkInputs()) {
    const memberName = document.getElementById("new-member-name").value;
    const memberVAT = document.getElementById("new-member-vat").value; // Changed field type
    let member = JSON.stringify({ name: memberName, vat: Number(memberVAT) });
    console.log("Sending member name " + JSON.stringify(member));
    const request = new XMLHttpRequest();
    request.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        window.location.href = "/sws-family.html";
      }
    });
    request.open("POST", "/cgi-bin/familyMembers");
    request.setRequestHeader("Content-Type", "application/json");
	  console.log(member);
    request.send(member);
  }
}
function purgeMembers() {
  const request = new XMLHttpRequest();
  request.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      window.location.href = "/sws-family.html";
    }
  });
  request.open("DELETE", "/cgi-bin/familyMembers");
  request.setRequestHeader("Content-Type", "text/plain");
  request.send("all");
}
function validateName(personName) {
  if (
    personName === null ||
    personName.length === 0 ||
    checkNameFormat(personName) === false
  ) {
    return false;
  }
  return true;
}

function validateVAT(vatNumber) {
  if (
    vatNumber === null ||
    vatNumber.length !== 9 ||
    checkVatFormat(vatNumber) === false
  ) {
    return false;
  }
  return true;
}

function checkNameFormat(name) {
  let nameFormat = /^[a-zA-ZÀ-ÿ-, ]+$/;
  let result = true;

  if (nameFormat.test(name) === false) {
    result = false;
  }
  return result;
}

function checkVatFormat(vat) {
  let vatFormat = /^[123][0-9]{8}$/;
  let result = true;

  if (vatFormat.test(vat) === false) {
    result = false;
  }
  return result;
}
function checkInputs() {
  const name = document.getElementById("new-member-name");
  const vat = document.getElementById("new-member-vat"); // Changed field type
  let status = true;
  if (!validateName(name.value)) {
    //show error
    setErrorFor(name, "Username cannot be blank");
    status = false;
  } else {
    setSuccessFor(name);
  }

  if (!validateVAT(vat.value)) {
    setErrorFor(vat, "VAT Number is invalid");
    status = false;
  } else {
    setSuccessFor(vat);
  }

  return status;
}
function setErrorFor(input, message) {
  const formControl = input.parentElement;
  const errorMessage = input.parentElement.getElementsByClassName(
    "error-message"
  )[0]; //get first and only object

  errorMessage.innerText = message;
  formControl.classList.add("error");
  errorMessage.classList.add("error");
}
function setSuccessFor(input) {
  const formControl = input.parentElement;
  const errorMessage = input.parentElement.getElementsByClassName(
    "error-message"
  )[0]; //get first and only object
  formControl.classList.remove("error");
  formControl.classList.add("success");
  errorMessage.classList.remove("error");
}
