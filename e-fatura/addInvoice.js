function addInvoice(e) {
    e.preventDefault();
    let invoiceDate = document.getElementById("date").value;
    let description = document.getElementById("description").value;
    let invoiceNumber = document.getElementById("invoice-number").value;
    let consumerVAT = document.getElementById("consumer-VAT").value;
    let providerVAT = document.getElementById("provider-VAT").value;
    let invoiceAmount = document.getElementById("invoice-amount").value;
    invoiceAmount = (Math.round(invoiceAmount * 100) / 100).toFixed(2);
    let invoiceCurrency = document.getElementById("invoice-currency").value;

    if (validateDate(invoiceDate) && validateDescription(description) && validateInvoiceNumber(invoiceNumber) && validateConsumerVAT(consumerVAT) && validateProviderVAT(providerVAT) && validateAmount(invoiceAmount) && notEquals(consumerVAT, providerVAT)) {
        console.log();

        const data = `invoice-date=${invoiceDate}&description=${description}&invoice-number=${invoiceNumber}&consumer-VAT=${consumerVAT}&provider-VAT=${providerVAT}&invoice-amount=${invoiceAmount}&invoice-currency=${invoiceCurrency}`;
        const invoiceRequest = new XMLHttpRequest();
        let responseText;
        invoiceRequest.onreadystatechange = function getResponse() {
            if (this.readyState === 4 && this.status === 200) {
                responseText = "The invoice was successfully added!";
                document.getElementById("response").innerHTML = "";
                document.getElementById("addInvoiceForm").innerHTML = responseText;
            }
            if (this.readyState === 4 && this.status === 403) {
                responseText = "The account with the consumer VAT doesn't exist. Please create it before adding an invoice";
                document.getElementById("response").innerHTML = responseText;
            }
            if (this.readyState === 4 && this.status === 400) {
                responseText = "Invalid request";
                document.getElementById("response").innerHTML = responseText;
            }
        };
        invoiceRequest.open("POST", "/cgi-bin/addInvoice.bash");
        invoiceRequest.send(data);
    }
}

function validateDate(date){
    date = Date.parse(date);
    let today = new Date();
    if(date > today){
        alert("The date cannot be superior to today's date!");
        return false;
    }
    return true;
}

function validateDescription(description) {
    if (description === null){
        alert("The description cannot be null!");
        return false;
    }
    return true;
}

function validateConsumerVAT(vatNumber) {
    if (vatNumber === null || vatNumber.length !== 9 || checkConsumerVatFormat(vatNumber) === false) {
        alert("The consumer VAT number is not valid");
        return false;
    }
    return true;
}

function validateProviderVAT(vatNumber) {
    if (vatNumber === null || vatNumber.length !== 9 || checkProviderVatFormat(vatNumber) === false) {
        alert("The provider VAT number is not valid");
        return false;
    }
    return true;
}

function notEquals(consumerVAT, providerVAT) {
    if(consumerVAT === providerVAT) {
        alert("The consumer VAT number and the provider VAT number cannot be the same")
        return false;
    }
    return true;
}

function checkConsumerVatFormat(vat) {
    let vatFormat = /^[123][0-9]{8}$/;
    let result = true;

    if (vatFormat.test(vat) === false) {
        result = false;
    }
    return result;
}

function checkProviderVatFormat(vat) {
    let vatFormat = /^[12356][0-9]{8}$/;
    let result = true;

    if (vatFormat.test(vat) === false) {
        result = false;
    }
    return result;
}

function validateAmount(amount) {
    if (amount === null || amount <= 0 || checkAmountFormat(amount) === false) {
        alert("The amount is not valid");
        return false;
    }
    return true;
}

function checkAmountFormat(amount) {
    let amountFormat = /^[0-9]*(.[0-9]{0,2})?$/;
    let result = true;

    if (amountFormat.test(amount) === false) {
        result = false;
    }
    return result;
}

function validateInvoiceNumber(invoiceNumber) {
    if (invoiceNumber === null || invoiceNumber <= 0 || checkInvoiceNumberFormat(invoiceNumber) === false) {
        alert("The invoice number is not valid.");
        return false;
    }
    return true;

}

function checkInvoiceNumberFormat(invoiceNumber) {
    let invoiceNumberFormat = /^[a-zA-Z0-9 /]*$/;
    let result = true;

    if (invoiceNumberFormat.test(invoiceNumber) === false) {
        result = false;
    }
    return result;
}
