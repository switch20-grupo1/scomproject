# SWS03 Automatically import data of connected external services to family member ledger

# 1. Requirements

_As a SWS family finance application prototype administrator, I want data of connected external services to be automatically imported to the family member’s ledger._

A Family Member is a user of this application, that was previously added by the family administrator.
The intended external services already are connected to the family members and now we want that each members ledger to be automatically updated with we data that is added on the external services. We think that make the update each hour is enough and we will add the functionality to refresh the ledger at anytime by the family administrator or member on each member html page.

A Ledger can be represented, as the following table:

| Date   | Description | Debit |  Credit | Balance |
| ------ | ----------- | ----: | ------: | ------: |
| 15-May | Salary      |       | 1896.34 | 1896.34 |
| 16-May | Groceries   | 98.54 |         | 1797.80 |

```plantuml
@startuml
header SSD
title Add a family member
autonumber
actor "Administrator" as Admin
participant ": Application" as App

Admin -> App : Automatically import external services data to family member ledger
activate Admin
activate App
return ask family member
deactivate App

Admin -> App : choose family member page
activate App

return inform success

deactivate Admin
@enduml
```

# 2. Analysis

## 2.1 Family Member

A Family Member has the following attributes:

| Attributes | Rules                                     |
| ---------- | ----------------------------------------- |
| name       | required, alphabetic, with spaces, String |
| VAT Number | required, numeric, 9 digits               |

## 2.2 API

There are only three types of API:

- E-Fatura;
- Finantial Services;
- Public Utilities.

## 2.2 API URI

The administrator needs to input the API URI that will follow the following structure: `http://<link-to-api.com>/?u=$memberVat`.

# 3. Design

## 3.1. Functionality Development

Regarding the creation of a new family member, we should accommodate the requirements
specified in [Analysis](#2-analysis).

The System Diagram is the following:

```plantuml
@startuml
header SD
title Add a family member and its ledger
autonumber
actor "System\nAdministrator" as Admin
participant "Webpage" as UI
participant "ledger.js" as JS
participant "fetchExternal.bash" as App

Admin -> UI : Automatically import external services\ndata to family member ledger
activate Admin
activate UI
return ask family member name
deactivate UI

Admin -> UI : choose family member
activate UI
UI -> JS : refresh Ledger
activate JS

JS -> JS : memberVat=document.getElementById\n("member-vat").textContent

JS -> App : request(GET, memberVat)
activate App
ref over App
    fetchExternal.bash
end ref
return ledger.csv
return ledger table
return present ledger table

deactivate Admin
@enduml
```

```plantuml
@startuml
header ref
title fetchExternal.bash
autonumber
participant "fetchExternal.bash" as BASH


[-> BASH : request(GET, memberVat)
activate BASH

BASH -> BASH : MEMBER=memberVat

loop for each api services.txt
BASH -> tempfile as "ledger-temp.csv" : curl GET ${ENDPOINT}
activate tempfile
    alt http_response == SUCCESS
        tempfile -> unsorted as "ledger-unsorted-test.csv"** : get relevant fields\n(using awk)
        activate unsorted
destroy tempfile
     end
end
unsorted -> sorted as "sorted.csv"** : sort_ledger()
activate sorted
destroy unsorted
sorted -> balance as "ledger.csv"** : balance_ledger()
activate balance
destroy sorted
balance --> BASH : ledger.cvs
deactivate balance

[<-- BASH: ledger.csv
deactivate BASH
@enduml
```

# 4. Implementation

For this US we made a script that will update all family members ledger at the same time and then we added this script to the `crontab` file at `/etc/crontab` so this script will be run automatically every hour of the day, every days during all year.

We also made a button on each `html` family member page so if the member or the administrator want to refresh the ledger at any time of the day, it can be done this way.

We opt to use `bc` to calculate the balance of each line and `awk` to get and sort the relevant table fields.

# 5. Integration/Demonstration

# 6. Comments
